let http = require('http');

let port = 3000; 

http.createServer( function(request, response) {
	response.write('This is me responding back');
	response.end();
}).listen(port);

console.log(`Server is running successfully on port: ${port}`);